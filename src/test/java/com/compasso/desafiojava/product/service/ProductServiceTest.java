package com.compasso.desafiojava.product.service;

import com.compasso.desafiojava.exception.ProductNotFoundException;
import com.compasso.desafiojava.product.dto.ProductDto;
import com.compasso.desafiojava.product.entity.Product;
import com.compasso.desafiojava.product.mapper.ProductMapper;
import com.compasso.desafiojava.product.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductServiceTest {

    @InjectMocks
    ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    @Mock
    private EntityManager entityManager;


    @Test
    void shouldCreateNewProduct() {
        ProductDto productDto = getProductDto();
        Product product = getProduct();
        Product savedProduct = getProduct();
        savedProduct.setId(UUID.randomUUID());
        ProductDto savedProductDto = getProductDto();
        savedProductDto.setId(savedProduct.getId());

        when(productMapper.toEntity(productDto)).thenReturn(product);
        when(productRepository.save(product)).thenReturn(savedProduct);
        when(productMapper.toDto(savedProduct)).thenReturn(savedProductDto);

        ProductDto result = productService.create(productDto);

        assertEquals(savedProductDto.getDescription(), result.getDescription());
        assertEquals(savedProductDto.getName(), result.getName());
        assertEquals(savedProductDto.getPrice(), result.getPrice());

        verify(productMapper).toEntity(productDto);
        verify(productRepository).save(product);
        verify(productMapper).toDto(savedProduct);
        verifyNoMoreInteractions(productMapper);
        verifyNoMoreInteractions(productRepository);
    }


    @Test
    void shouldUpdateExistingProduct() {
        UUID productId = UUID.randomUUID();
        ProductDto productToUpdate = getProductDtoToUpdate(productId);
        Product product = getProduct();
        product.setId(productId);

        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
        when(productRepository.save(any(Product.class))).thenReturn(product);

        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);

        productService.update(productToUpdate.getId(), productToUpdate);

        verify(productRepository).save(captor.capture());
        verify(productMapper, times(2)).toDto(captor.capture());

    }

    @Test
    void shouldThrowExceptionWhenTryToUpdateNonexistentProduct() {
        UUID productId = UUID.randomUUID();
        ProductDto productToUpdate = getProductDtoToUpdate(productId);
        assertThrows(ProductNotFoundException.class,
                () -> productService.update(productToUpdate.getId(), productToUpdate));
        verify(productRepository).findById(productId);
        verifyNoMoreInteractions(productRepository);
    }

    @Test
    void shouldFindById() {
        UUID productId = UUID.randomUUID();
        Product product = getProduct();
        product.setId(productId);
        ProductDto productDto = getProductDto();
        productDto.setId(productId);

        when(productMapper.toDto(product)).thenReturn(productDto);
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        productService.findById(productId);

        verify(productRepository).findById(productId);
        verifyNoMoreInteractions(productRepository);
    }

    @Test
    void shouldFindAll() {
        UUID uuid = UUID.randomUUID();
        UUID uuidTwo = UUID.randomUUID();
        Product product = getProduct();
        product.setId(uuid);
        Product productTwo = getProduct();
        productTwo.setId(uuidTwo);

        when(productRepository.findAll()).thenReturn(List.of(product, productTwo));

        List<ProductDto> result = productService.findAll();

        assertEquals(2, result.size());
        verify(productMapper).toDto(product);
        verify(productMapper).toDto(productTwo);
        verify(productRepository).findAll();
        verifyNoMoreInteractions(productRepository);
    }

    @Test
    void shouldReturnEmptyListWhenNotFoundProducts() {
        when(productRepository.findAll()).thenReturn(new ArrayList<>());
        List<ProductDto> result = productService.findAll();

        assertEquals(0, result.size());

        verify(productRepository).findAll();
        verifyNoMoreInteractions(productRepository);
        verifyNoInteractions(productMapper);

    }

    @Test
    void shouldDeleteById() {
        UUID productId = UUID.randomUUID();
        Product product = getProduct();
        product.setId(productId);

        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        productService.deleteById(productId);

        verify(productRepository).findById(productId);
        verify(productRepository).deleteById(productId);
        verifyNoMoreInteractions(productRepository);

    }

    @Test
    void shouldFilterProductsWithAllArgs() {
        String description = "Caderno";
        BigDecimal minPrice = new BigDecimal(10);
        BigDecimal maxPrice = new BigDecimal(20);
        String query = "SELECT p.id id, p.name name, p.description description, p.price price FROM product p " +
                "WHERE 1 = 1 " +
                "AND (LOWER(p.name) LIKE %:description% OR LOWER(p.description) LIKE %:description%) " +
                "AND :minPrice >= p.price " +
                "AND :maxPrice <= p.price";
        Product product = getProduct();

        when(productRepository.findAll()).thenReturn(List.of(product));
        when(entityManager.createNativeQuery(any(String.class), eq(Product.class))).thenReturn(mock(Query.class));

        productService.search(description, minPrice, maxPrice);

        verify(entityManager).createNativeQuery(query, Product.class);
        verifyNoMoreInteractions(entityManager);
    }

    @Test
    void shouldFilterProductsWithDescription() {
        String description = "Caderno";
        String query = "SELECT p.id id, p.name name, p.description description, p.price price FROM product p " +
                "WHERE 1 = 1 " +
                "AND (LOWER(p.name) LIKE %:description% OR LOWER(p.description) LIKE %:description%) ";
        Product product = getProduct();

        when(productRepository.findAll()).thenReturn(List.of(product));
        when(entityManager.createNativeQuery(any(String.class), eq(Product.class))).thenReturn(mock(Query.class));

        productService.search(description, null, null);

        verify(entityManager).createNativeQuery(query, Product.class);
        verifyNoMoreInteractions(entityManager);
    }


    private ProductDto getProductDtoToUpdate(UUID productId) {
        ProductDto productToUpdate = new ProductDto();
        productToUpdate.setPrice(new BigDecimal(22));
        productToUpdate.setName("Caderno Atualizado");
        productToUpdate.setDescription("Novo Caderno Atualizado");
        productToUpdate.setId(productId);
        return productToUpdate;
    }

    private ProductDto getProductDto() {
        ProductDto productDto = new ProductDto();
        productDto.setDescription("Novo Caderno");
        productDto.setName("Caderno");
        productDto.setPrice(new BigDecimal(12));
        return productDto;
    }

    private Product getProduct() {
        Product product = new Product();
        product.setDescription("Novo Caderno");
        product.setName("Caderno");
        product.setPrice(new BigDecimal(12));
        return product;
    }

}
