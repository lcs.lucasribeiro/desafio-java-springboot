package com.compasso.desafiojava.exception;

import lombok.Data;

@Data
public class Response {

    private int status_code;
    private String message;

}
