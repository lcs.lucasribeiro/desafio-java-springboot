package com.compasso.desafiojava.product.service;

import com.compasso.desafiojava.exception.ProductNotFoundException;
import com.compasso.desafiojava.product.dto.ProductDto;
import com.compasso.desafiojava.product.entity.Product;
import com.compasso.desafiojava.product.mapper.ProductMapper;
import com.compasso.desafiojava.product.repository.ProductRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductMapper productMapper;
    private final ProductRepository productRepository;
    private final EntityManager entityManager;

    public ProductService(ProductMapper productMapper, ProductRepository productRepository, EntityManager entityManager) {
        this.productMapper = productMapper;
        this.productRepository = productRepository;
        this.entityManager = entityManager;
    }

    public ProductDto create(ProductDto newProduct) {
        Product product = productMapper.toEntity(newProduct);
        Product savedProduct = productRepository.save(product);
        return productMapper.toDto(savedProduct);
    }


    public ProductDto update(UUID id, ProductDto newProduct) {
        findById(id);
        Product product = getProduct(id, newProduct);
        Product saved = productRepository.save(product);
        return productMapper.toDto(saved);
    }

    private Product getProduct(UUID id, ProductDto newProduct) {
        Product product = new Product();
        product.setId(id);
        product.setDescription(newProduct.getDescription());
        product.setName(newProduct.getName());
        product.setPrice(newProduct.getPrice());
        return product;
    }

    public ProductDto findById(UUID id) {
        Product product = productRepository.findById(id).orElseThrow(ProductNotFoundException::new);
        return productMapper.toDto(product);
    }

    public List<ProductDto> findAll() {
        List<Product> products = productRepository.findAll();
        if (products.isEmpty()) {
            return new ArrayList<>();
        }
        return products.stream().map(productMapper::toDto).collect(Collectors.toList());
    }

    public void deleteById(UUID id) {
        findById(id);
        productRepository.deleteById(id);
    }

    @SuppressWarnings("unchecked")
    public List<ProductDto> search(String description, BigDecimal minPrice, BigDecimal maxPrice) {
        StringBuilder sql = new StringBuilder();
        Map<String, String> parameters = new HashMap<>();
        sql.append("SELECT p.id id, p.name name, p.description description, p.price price FROM product p WHERE 1 = 1 ");
        if (Objects.nonNull(description)) {
            sql.append("AND (LOWER(p.name) LIKE %:description% OR ");
            sql.append("LOWER(p.description) LIKE %:description%) ");
            parameters.put("description", description.toLowerCase());
        }
        if (Objects.nonNull(minPrice)) {
            sql.append("AND :minPrice >= p.price ");
            parameters.put("minPrice", minPrice.toString());
        }
        if (Objects.nonNull(maxPrice)) {
            sql.append("AND :maxPrice <= p.price");
            parameters.put("maxPrice", maxPrice.toString());
        }
        Query nativeQuery = entityManager.createNativeQuery(sql.toString(), Product.class);
        parameters.forEach(nativeQuery::setParameter);
        List<Product> resultList = nativeQuery.getResultList();
        return resultList.stream().map(productMapper::toDto).collect(Collectors.toList());
    }
}
