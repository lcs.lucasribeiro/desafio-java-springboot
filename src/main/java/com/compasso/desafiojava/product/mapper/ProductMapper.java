package com.compasso.desafiojava.product.mapper;

import com.compasso.desafiojava.product.dto.ProductDto;
import com.compasso.desafiojava.product.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {

    ProductDto toDto(Product product);

    Product toEntity(ProductDto productDto);

}
