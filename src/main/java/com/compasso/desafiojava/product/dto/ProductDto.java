package com.compasso.desafiojava.product.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@Data
public class ProductDto {

    private UUID id;
    @NotEmpty(message = "Field name must be send")
    private String name;
    @NotEmpty(message = "Field description must be send")
    private String description;
    @NotNull(message = "Field price must be send")
    private BigDecimal price;
}
