package com.compasso.desafiojava.product.controller;

import com.compasso.desafiojava.product.dto.ProductDto;
import com.compasso.desafiojava.product.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDto create(@Valid @RequestBody ProductDto newProduct) {
        return productService.create(newProduct);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto update(@PathVariable("id") UUID id,
                             @Valid @RequestBody ProductDto product) {
        return productService.update(id, product);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto findById(@PathVariable("id") UUID id) {
        return productService.findById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductDto> findAll() {
        return productService.findAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") UUID id) {
        productService.deleteById(id);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductDto> filter(@RequestParam(value = "q", required = false) String description,
                                   @RequestParam(value = "min_price", required = false) BigDecimal minPrice,
                                   @RequestParam(value = "max_price", required = false) BigDecimal maxPrice) {
        return productService.search(description, minPrice, maxPrice);
    }

}
